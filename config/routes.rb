Rails.application.routes.draw do
 
  
  #facebookログイン用のルート、ちょっとバグが出るのでコメント
  devise_for :users,
  path: '',
  path_names: {sign_in: 'login', sign_out: 'logout', edit: 'profile', sign_up: 'registration'},
  controllers: {omniauth_callbacks: 'omniauth_callbacks'}
  
  
  
  root   'static_pages#home'
  
  
  #いいねをした時のルート
  post "likes/:post_id/create" => "likes#create"
  post "likes/:post_id/destroy" => "likes#destroy"

  #ユーザーに関するルート群
  post "users/:id/update" => "users#update"
  get "users/:id/edit" => "users#edit"
  post "users/create" => "users#create"
  get "signup" => "users#new"
  get "users/index" => "users#index"
  get "users/:id" => "users#show"
  post "login" => "users#login"
  post "logout" => "users#logout"
  get "login" => "users#login_form"
  get "users/:id/likes" => "users#likes"
  #下に別ルート設定
  #get "users/:id/following" => "users#following"
  #get "users/:id/followers" => "users#followers"

  #ポスト関係に関するルート群
  get "posts/index" => "posts#index"
  get "posts/new" => "posts#new"
  get "posts/:id" => "posts#show"
  post "posts/create" => "posts#create"
  get "posts/:id/edit" => "posts#edit"
  post "posts/:id/update" => "posts#update"
  post "posts/:id/destroy" => "posts#destroy"

#ホームに関するルート群
  get "/" => "home#top"
  #get "about" => "home#about"
  
  #follow resource
  resources :users do
    member do
      get :following, :followers
    end
  end

  #resources群
  resources :users
  #resources :account_activations, only[:edit]
  resources :relationships, only: [:create, :destroy]
end