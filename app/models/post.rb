class Post < ApplicationRecord
  belongs_to :user, optional: true
  default_scope -> { order(created_at: :desc) }
  scope :search_by_keyword, -> (keyword) {
    where("posts.content LIKE :keyword", keyword: "%#{sanitize_sql_like(keyword)}%") if keyword.present?}
  #picture upload method
  mount_uploader :picture, PictureUploader
  validates :user_id, {presence: true}
  validates :content, {presence: true, length: {maximum: 140}}
  #エラーが出てしまうので別方法で画像処理
  #validate :picture_size
  
  
  def picture_size
    if picture_size > 5.megabytes
      error.add(:picture, "should be less than 5MB")
    end
  end   
end
