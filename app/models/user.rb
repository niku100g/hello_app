class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :rememberable, :omniauthable, omniauth_providers: [:facebook]
  #userkからも検索するよ
  scope :search_by_keyword, -> (keyword) {
    where("users.name LIKE :keyword", keyword: "%#{sanitize_sql_like(keyword)}%") if keyword.present?
  }
  before_create :create_activation_digest
  attr_accessor :remember_token, :activation_token, :encrypted_password, :password, :password_confirmation    
  before_save { self.email = email.downcase }
  validates :name,  {presence: true, length: { maximum: 50 }}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i 
  validates :email, {presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: true 
  }
  validates :password, {presence: true, confirmation: true, length: { maximum: 6}}
  has_many :posts, dependent: :destroy
  has_many :active_relationships,  class_name:  "Relationship",
                                   foreign_key: "follower_id",
                                   dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  has_many :following, through: :active_relationships,  source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  # 渡された文字列のハッシュ値を返す
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end     
  
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  def downcase_email
      self.email = email.downcase
  end
  
  def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
  end
  
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  def new_token
      SecureRandom.urlsafe_base64
  end
  
   def self.from_omniauth(auth)
    @user = User.where(email: auth.info.email).first
    if @user
      return @user
    else
      where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        # userモデルが持っているカラムをそれぞれ定義
        @user.email = auth.info.email
        @user.password = Devise.friendly_token[0,20]
        @user.name = auth.info.name
        @user.image = auth.info.image
        @user.uid = auth.uid
        @user.provider = auth.provider

        @user.skip_confirmation!
      end
    end
    
    
    users
   end
   
   def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
   end

  # ユーザーをフォロー解除する
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # 現在のユーザーがフォローしてたらtrueを返す
  def following?(other_user)
    following.include?(other_user)
  end

 
  
  def posts
    return Post.where(user_id: self.id)
  end
  
  
end
