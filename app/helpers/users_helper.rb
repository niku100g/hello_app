module UsersHelper
  
  def login(user)
    
   session[:user_id] = @user.id
  end
  
  def remember(user)
    remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  def logout(user)
    forget(current_user)
    session.delete(:user_id)
    session[:user_id] = nil
  end
  
  def forget(user)
    forget(user)
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
  
end
