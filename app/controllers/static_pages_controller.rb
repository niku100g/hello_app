class StaticPagesController < ApplicationController
  def home
    if current_user
     @post  = current_user.posts.build
      if params[:q]
        relation = Post.joins(:user)
        @feed_items = relation.merge(User.search_by_keyword(params[:q]))
                        .or(relation.search_by_keyword(params[:q]))
                        .paginate(page: params[:page])
      end
    end
  end

  def help
  end
end
