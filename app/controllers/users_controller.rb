class UsersController < ApplicationController
  before_action :authenticate_user, {only: [:index, :show, :edit, :update, :following, :followers, :logout]}
  before_action :forbid_login_user, {only: [:new, :create, :login_form, :login]}
  before_action :ensure_correct_user, {only: [:edit, :update]}
  
  def index
    @users = User.all
    @users = User.paginate(page: params[:page])
  end
  
  #ページネーション
  def show
    @user = User.find_by(id: params[:id])
  end
  
  def new
    @user = User.new
  end
  
  #ユーザー登録
  def create
    @user = User.new(#user_params,
      name: params[:name],
      email: params[:email],
      image_name: "default_user.jpg",
      password: params[:password],
      password_confirmation: params[:password_confirmation]
    )
    binding.pry
    if @user.save
      session[:user_id] = @user.id
      flash[:success] = "ユーザー登録が完了しました"
      redirect_to("/users/#{@user.id}")
    else
      render("users/new")
    end
  end
  
  #def user_params
    #params.require(:user).permit(:name, :email, :password, :password_confirmation)
  #end
  
  def edit
    @user = User.find_by(id: params[:id])
  end
  
  #ユーザー情報の更新
  def update
    @user = User.find_by(id: params[:id])
    @user.name = params[:name]
    @user.email = params[:email]
    
    if params[:image]
      @user.image_name = "#{@user.id}.jpg"
      image = params[:image]
      File.binwrite("public/user_images/#{@user.image_name}", image.read)
    end
    
    if @user.save
      flash[:success] = "ユーザー情報を編集しました"
      redirect_to("/users/#{@user.id}")
    else
      render("users/edit")
    end
  end
  
  def login_form
  end
  
  #ログインする
  def login
    @user = User.find_by(email: params[:email].downcase)
    if @user 
      session[:user_id] = @user.id
      cookies.permanent.signed[:user_id] = @user.id
      cookies.permanent[:remember_token] = @user.remember_token
      flash[:success] = "ログインしました"
      params[:remember_me] == '1' ? remember(@user) : forget(@user)
      redirect_to("/posts/index")
    else
      @error_message = "メールアドレスまたはパスワードが間違っています"
      @email = params[:email]
      @password = params[:password]
      render("users/login_form")
    end
  end
  
  def forget
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
  
   def remember
    remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
   end
  
  def logout
    session[:user_id] = nil
    flash[:success] = "ログアウトしました"
    redirect_to("/login")
  end
  
  #ユーザーのいいね数
  def likes
    @user = User.find_by(id: params[:id])
    @likes = Like.where(user_id: @user.id)
  end
  
  #フォローしてる数
  def following
    @title = "Following"
    @user  = User.find_by(params[:id])
    @users = @user.following.paginate(page: params[:page])
  end

  #フォローされている数
  def followers
    @title = "Followers"
    @user  = User.find_by(params[:id])
    @users = @user.followers.paginate(page: params[:page])
  end
  
  #ユーザー情報を編集したり、更新する権利がユーザーにあるか判断するよ
  def ensure_correct_user
    if @current_user.id != params[:id].to_i
      flash[:warning] = "権限がありません"
      redirect_to("/posts/index")
    end
  end
  
  
  
end
