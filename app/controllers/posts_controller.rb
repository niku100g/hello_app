class PostsController < ApplicationController
  before_action :authenticate_user
  before_action :ensure_correct_user, {only: [:edit, :update, :destroy]}
  
  #投稿一覧（昇降順）
  def index
    @posts = Post.all.order(created_at: :desc)
    #Paginate method
    @posts = Post.paginate(page: params[:page])
  end
  
  #投稿記事を確認するよ、いいね数もカウントする
  def show
    @post = Post.find_by(id: params[:id])
    @user = @post.user
    @likes_count = Like.where(post_id: @post.id).count
    #Paginate method
    @posts = Post.paginate(page: params[:page])
  end
  
  def search
  #ViewのFormで取得したパラメータをモデルに渡す
    @posts = Post.search(params[:search])
  end
  
  def new
    @post = Post.new
  end
  
  #投稿を作成する
  def create
    @post = Post.new(
      content: params[:content],
      picture: params[:picture],
      user_id: @current_user.id
    )
    
    #別の方法で画像のuploadに変える
    #if params[:picture] != nil
        #@post.picture = "#{@post.id}.jpg"
        #picture = MiniMagick::Image.read(params[:picture])
        #picture.resize "400x400"
        #picture.write("public/post_images/#{@post.picture}")
    #end
    
    if @post.save
      flash[:success] = "投稿を作成しました"
      redirect_to("/posts/index")
    else
      @error_message = "投稿のコンテンツがありません"
      render("posts/new")
    end
  end
  
  
  def edit
    @post = Post.find_by(id: params[:id])
  end
  
  #投稿を編集するよ
  def update
    @post = Post.find_by(id: params[:id])
    @post.content = params[:content]
    
    
    if @post.save
      flash[:success] = "投稿を編集しました"
      redirect_to("/posts/index")
    else
      render("posts/edit")
    end
  end
  
  #投稿を削除
  def destroy
    @post = Post.find_by(id: params[:id])
    @post.destroy
    flash[:info] = "投稿を削除しました"
    redirect_to("/posts/index")
  end
  
  #画像を投稿を許可する
  def post_params
    params.require(:post).permit(:content, :picture)
  end
  
  #投稿者でないと投稿内容を編集できないようにする
  def ensure_correct_user
    @post = Post.find_by(id: params[:id])
    if @post.user_id != @current_user.id
      flash[:warning] = "権限がありません"
      redirect_to("/posts/index")
    end
  end
  
end
