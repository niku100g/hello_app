class ApplicationController < ActionController::Base
  before_action :set_current_user
  #before_action :authenticate_user! 
  add_flash_types :success, :info, :warning, :danger
  
  def set_current_user
    if (user_id = session[:user_id])
     @current_user ||= User.find_by(id: session[:user_id])
      else if (user_id = cookies.signed[user_id])
        user = User.find_by(id: user_id)
          if user && user.authenticated?(cookies[:remember_token])
             log_in user
             @current_user = user
          end
      end
    end
  end
  
  
  def authenticate_user
    if @current_user == nil
      flash[:warning] = "ログインが必要です"
      redirect_to("/login")
    end
  end
  
  def forbid_login_user
    if @current_user
      flash[:info] = "すでにログインしています"
      redirect_to("/posts/index")
    end
  end

end
